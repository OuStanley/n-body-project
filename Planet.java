public class Planet {

    public double xxPos;
    public double yyPos;
    public double xxVel;
    public double yyVel;
    public double mass;
    public String imgFileName;
    
    public Planet(double xP, double yP, double xV,
                  double yV, double m, String img) {

        this.xxPos = xP;
        this.yyPos = yP;
        this.xxVel = xV;
        this.yyVel = yV;
        this.mass = m;
        this.imgFileName = img;
        
    }

    public Planet(Planet p) {

        this.xxPos = p.xxPos;
        this.yyPos = p.yyPos;
        this.xxVel = p.xxVel;
        this.yyVel = p.yyVel;
        this.mass = p.mass;
        this.imgFileName = p.imgFileName;   
        
    }

    public double calcDistance(Planet p) {
        
        double dx_squared;
        double dy_squared;
        double r_squared;
        double DistanceBetweenPlanets;

        dx_squared = (this.xxPos - p.xxPos) * (this.xxPos - p.xxPos);
        dy_squared = (this.yyPos - p.yyPos) * (this.yyPos - p.yyPos);
        r_squared = dx_squared + dy_squared;
        DistanceBetweenPlanets = Math.sqrt(r_squared);
        
        return DistanceBetweenPlanets;     
        
    }

    public double calcForceExertedBy(Planet p) {
        double GravitationalConstant = (6.67e-11);
        double r = p.calcDistance(this);
        double r_squared = r * r;

        return (this.mass * p.mass * GravitationalConstant) / (r_squared);

    }

    public double calcForceExertedByX(Planet p) {
        double ForceExerted;
        double dx;
        double radius;

        ForceExerted = this.calcForceExertedBy(p);
        dx = (this.xxPos - p.xxPos);
        radius = this.calcDistance(p);

        return -1 * (ForceExerted * dx) / radius;
          
    }
    

    public double calcForceExertedByY(Planet p) {
        double ForceExerted;
        double dy;
        double radius;

        ForceExerted = this.calcForceExertedBy(p);
        dy = (this.yyPos -  p.yyPos);
        radius = this.calcDistance(p);

        return -1 * (ForceExerted * dy) / radius;
        
    }

    
    public double calcNetForceExertedByX(Planet[] ArrayOfPlanets) {
        double NetForceX = 0;
        for (int i = 0; i < ArrayOfPlanets.length; i = i + 1) {
            if (this.equals(ArrayOfPlanets[i])) {
                NetForceX = NetForceX;
            }
            else {               
                NetForceX = NetForceX + this.calcForceExertedByX(ArrayOfPlanets[i]);
            }
        }
        return NetForceX;
   
    }

    public double calcNetForceExertedByY(Planet[] ArrayOfPlanets) {
        double NetForceY = 0;
        for (int i = 0; i < ArrayOfPlanets.length; i = i + 1) {
            if (this.equals(ArrayOfPlanets[i])) {
                NetForceY = NetForceY;
            }
            else {               
                NetForceY = NetForceY + this.calcForceExertedByY(ArrayOfPlanets[i]);
            }
        }
        return NetForceY;
    }

    
    public void update(double dt, double fX, double fY) {
        double NetAccelerationX = (fX / this.mass);
        double NetAccelerationY = (fY / this.mass);
        
        this.xxVel = this.xxVel + (dt * NetAccelerationX);
        this.yyVel = this.yyVel + (dt * NetAccelerationY);
        this.xxPos = this.xxPos + (dt * this.xxVel);
        this.yyPos = this.yyPos + (dt * this.yyVel);  
        
    }

    public void draw() {
        StdDraw.picture(xxPos, yyPos, "images/" + imgFileName);

    }
        
    
}
