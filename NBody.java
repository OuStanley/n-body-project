
public class NBody {
    
    public static double readRadius(String file) {
        In in = new In(file);
        int NumberOfPlanets = in.readInt();
        double radius = in.readDouble();
        return radius;
        
    }
    
    public static Planet[] readPlanets(String file) {
        In in = new In(file);
        int NumberOfPlanets = in.readInt();
        double radius = in.readDouble();
        Planet[] ArrayOfPlanets = new Planet[NumberOfPlanets];
        int count = 0;
        while (!in.isEmpty() && count != NumberOfPlanets) {
            double xP = in.readDouble();
            double yP = in.readDouble();
            double xV = in.readDouble();
            double yV = in.readDouble();
            double m = in.readDouble();
            String img = in.readString();
            ArrayOfPlanets[count] = new Planet(xP, yP, xV, yV, m, img);
            count = count + 1;
            
        }

        return ArrayOfPlanets; 

    }

    public static void main(String[] args) {
        double T = Double.parseDouble(args[0]);
        double dt = Double.parseDouble(args[1]);
        String filename = args[2];
        double radius = readRadius(filename);
        Planet[] ArrayOfPlanets = readPlanets(filename);

        String BackgroundImage = "images/starfield.jpg";
        StdDraw.setScale(-radius, radius);
        StdDraw.clear();
        StdDraw.picture(0, 75, BackgroundImage);
        StdDraw.picture(-75, 75, BackgroundImage);
        StdDraw.picture(75, -75, BackgroundImage);

        for (int i = 0; i < ArrayOfPlanets.length; i += 1) {
            ArrayOfPlanets[i].draw();
        }

        StdDraw.enableDoubleBuffering();
        double TimeVariable = 0;
        while (TimeVariable != T) {
            double[] xForces = new double[ArrayOfPlanets.length];
            double[] yForces = new double[ArrayOfPlanets.length];
            for (int i = 0; i < ArrayOfPlanets.length; i += 1) {
                xForces[i] = ArrayOfPlanets[i].calcNetForceExertedByX(ArrayOfPlanets);
                yForces[i] = ArrayOfPlanets[i].calcNetForceExertedByY(ArrayOfPlanets);
            }

            for (int i = 0; i < ArrayOfPlanets.length; i += 1) {
                ArrayOfPlanets[i].update(dt, xForces[i], yForces[i]);

            }
            StdDraw.setScale(-radius, radius);
            StdDraw.clear();
            StdDraw.picture(0, 75, BackgroundImage);

            for (int i = 0; i < ArrayOfPlanets.length; i += 1) {
                ArrayOfPlanets[i].draw();
            }

            StdDraw.show();
            StdDraw.pause(10);
            TimeVariable = TimeVariable + dt;
         
            
        }
        StdOut.printf("%d\n", ArrayOfPlanets.length);
        StdOut.printf("%.2e\n", radius);
        for (int i = 0; i < ArrayOfPlanets.length; i++) {
            StdOut.printf("%11.4e %11.4e %11.4e %11.4e %11.4e %12s\n",
                          ArrayOfPlanets[i].xxPos, ArrayOfPlanets[i].yyPos, ArrayOfPlanets[i].xxVel,
                          ArrayOfPlanets[i].yyVel, ArrayOfPlanets[i].mass, ArrayOfPlanets[i].imgFileName);
            
        }
        
    }
         
}
